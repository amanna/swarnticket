//
//  DashboardVC.m
//  SwarnTicket
//
//  Created by Aditi on 23/06/16.
//  Copyright © 2016 ABC. All rights reserved.
//

#import "DashboardVC.h"
#import "StaffOrganizerDetails.h"
@interface DashboardVC ()
@property (weak, nonatomic) IBOutlet UIButton *btnStaff;
@property (weak, nonatomic) IBOutlet UIButton *btnOrganizer;
@property (weak, nonatomic) IBOutlet UIButton *btnEvent;
- (IBAction)btnStaffAction:(UIButton *)sender;
- (IBAction)btnOrganizerAction:(UIButton *)sender;
- (IBAction)btnEventAction:(UIButton *)sender;
@property(nonatomic)NSInteger isStaff;

@property (weak, nonatomic) IBOutlet UIButton *btnLogout;

- (IBAction)btnLogoutAction:(UIButton *)sender;


@end

@implementation DashboardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.btnStaff.layer.borderWidth = 2.0f;
    self.btnStaff.layer.borderColor = [UIColor whiteColor].CGColor;
    self.btnStaff.layer.cornerRadius = 4.0f;
    
    self.btnOrganizer.layer.borderWidth = 2.0f;
    self.btnOrganizer.layer.borderColor = [UIColor whiteColor].CGColor;
    self.btnOrganizer.layer.cornerRadius = 4.0f;
    
    self.btnEvent.layer.borderWidth = 2.0f;
    self.btnEvent.layer.borderColor = [UIColor whiteColor].CGColor;
    self.btnEvent.layer.cornerRadius = 4.0f;
    
    
    self.btnLogout.layer.borderWidth = 2.0f;
    self.btnLogout.layer.borderColor = [UIColor whiteColor].CGColor;
    self.btnLogout.layer.cornerRadius = 4.0f;

    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"seguestaffOrganizer"]) {
        StaffOrganizerDetails *vc = [segue destinationViewController];
        vc.isStaff = self.isStaff;
        
    }
}
- (IBAction)btnStaffAction:(UIButton *)sender {
    self.isStaff = 1;
    [self performSegueWithIdentifier:@"seguestaffOrganizer" sender:self];
}

- (IBAction)btnOrganizerAction:(UIButton *)sender {
     self.isStaff = 0;
     [self performSegueWithIdentifier:@"seguestaffOrganizer" sender:self];
}

- (IBAction)btnEventAction:(UIButton *)sender {
     [self performSegueWithIdentifier:@"segueEventDash" sender:self];
}
- (IBAction)btnLogoutAction:(UIButton *)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
