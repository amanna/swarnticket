//
//  LoginVC.m
//  SwarnTicket
//
//  Created by Aditi on 23/06/16.
//  Copyright © 2016 ABC. All rights reserved.
//

#import "LoginVC.h"
#import <QuartzCore/QuartzCore.h>
@interface LoginVC ()
- (IBAction)btnLoginAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *btnLogin;

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.btnLogin.layer.borderWidth = 2.0f;
    self.btnLogin.layer.borderColor = [UIColor whiteColor].CGColor;
    self.btnLogin.layer.cornerRadius = 4.0f;
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnLoginAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"segeDashboard" sender:self];
}
@end
