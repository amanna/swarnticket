//
//  StaffOrganizerDetails.m
//  SwarnTicket
//
//  Created by Aditi on 23/06/16.
//  Copyright © 2016 ABC. All rights reserved.
//

#import "StaffOrganizerDetails.h"

@interface StaffOrganizerDetails ()
@property (weak, nonatomic) IBOutlet UILabel *lblStaffDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblPhone;
@end

@implementation StaffOrganizerDetails
- (IBAction)btnBackAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    if(self.isStaff==1){
        self.lblStaffDetails.text = @"Staff Details";
    }else{
         self.lblStaffDetails.text = @"Organizer Details";
    }
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
