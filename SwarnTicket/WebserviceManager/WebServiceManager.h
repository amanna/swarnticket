//
//  WebServiceManager.h
//  WovaxUniversalApp
//
//  Created by Susim Samanta on 11/11/13.
//  Copyright (c) 2013 Wovax, LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^FetchCompletionHandler)(id object, NSError *error);
typedef void (^FetchCompletionHandler1)(id object,id object1, NSError *error);
@interface WebServiceManager : NSObject
+(void)loginOnCompletion:(NSString*)strUserName andPass:(NSString*)strPass onCompletion:(FetchCompletionHandler)handler;
+(void)getEventDetails:(NSString*)strUid onCompletion:(FetchCompletionHandler)handler;
+(void)getOrgDetails:(NSString*)strUid onCompletion:(FetchCompletionHandler)handler;
+(void)getscan:(NSString*)strUid onCompletion:(FetchCompletionHandler)handler;
+(void)getVisit:(NSString*)strUid onCompletion:(FetchCompletionHandler)handler;


+(void)logoutOnCompletion:(NSString*)strUid onCompletion:(FetchCompletionHandler)handler;
+(void)agentCreation:(NSString*)strUid andEmpName:(NSString*)strEmpName andEmpMail:(NSString*)strEmail  onCompletion:(FetchCompletionHandler)handler;
+(void)getAgentList:(NSString*)strUid onCompletion:(FetchCompletionHandler)handler;
+(void)createInvoice:(NSString*)strInvoice onCompletion:(FetchCompletionHandler)handler;
+(void)getInvoiceList:(NSString*)strEmp onCompletion:(FetchCompletionHandler)handler;


+(void)addEmployee:(NSString*)strEmp onCompletion:(FetchCompletionHandler)handler;
+(void)EditEmployee:(NSString*)strEmp onCompletion:(FetchCompletionHandler)handler;
+(void)deleteEmployee:(NSString*)strEmp onCompletion:(FetchCompletionHandler1)handler;
+(void)EditAgent:(NSString*)strEmp onCompletion:(FetchCompletionHandler)handler;
+(void)deleteAgent:(NSString*)strEmp onCompletion:(FetchCompletionHandler)handler;
+(void)getEmpListonCompletion:(FetchCompletionHandler)handler;
+(void)sendExcel:(NSString*)strEmp onCompletion:(FetchCompletionHandler)handler;
+(void)deleteInvoice:(NSString*)strInvoice onCompletion:(FetchCompletionHandler)handler;
+(void)getSettingsInfo:(FetchCompletionHandler)handler;
@end

//{"Success":true,"msg":"Login successfully","User_Details":{"Name":"Chandrahas","TargetId":"2022","UserPoints":5647,"Tire":"Platinum"}}

//offer list
//{"Success":true,"msg":"Offer Found","OfferList":[{"OfferId":11,"OfferName":"Free Play Cash!","OfferDescription":"Free Play Cash","OfferCode":"FPC","OfferValidityPeriod":"","OfferExpDate":"1\/15\/2017 12:00:00 AM","OfferImage":"https:\/\/landingpage.experiture.com\/TrackingSystem\/AG_44625\/AD_44626\/Images\/all_off_1.jpg"}]}


//URL: http://lab-2.sketchdemos.com/P-603-globehero/

//1.url : api/login
//method : POST
//data : email,password
//resposnse :  result:
//message , data,
//status: 0 -> No Data Found
//1 -> Login Successful
//2 -> Invalid Email or Password
//
//
//2.url : api/eventdetails
//method : POST
//data : id
//resposnse :  result:
//message , data,
//status: 0 -> No Data Found
//1 -> Event's Detail
//2 -> No Event Found
//
//
//3.url : api/orgdetails
//method : POST
//data : id
//resposnse :  result:
//message , data,
//status: 0 -> No Data Found
//1 -> Organizer's Detail
//2 -> No Organizer Found
//
//4.url : api/scan
//method : POST
//data : qrcode OR ticketno
//resposnse :  result:
//message ,
//status: 0 -> No Data Found
//1 -> Valid QRcode
//2 -> Invalid QRcode
//3 -> Valid Ticket Number
//4 -> Invalid Ticket Number
//
//5.url : api/visit
//method : POST
//data : qrcode OR ticketno
//resposnse :  result:
//message ,
//status: 0 -> No Data Found
//1 -> Visit
//2 -> Can't Visit
//3 -> Invalid QRcode or Already Visited
//4 -> Invalid Ticket Number
//5 -> Already Visited