//
//  WebServiceManager.m
//  WovaxUniversalApp
//
//  Created by Susim Samanta on 11/11/13.
//  Copyright (c) 2013 Wovax, LLC. All rights reserved.
//

#import "WebServiceManager.h"
#import "SSRestManager.h"

@implementation WebServiceManager
+(void)loginOnCompletion:(NSString*)strUserName andPass:(NSString*)strPass onCompletion:(FetchCompletionHandler)handler{
  //  NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kLogin];
    
    NSString *strLoginUrl = @"";
  //strUserName = @"job2012.aditi@gmail.com";
 //strPass= @"123456";
   
    
     NSString *kQuery = [NSString stringWithFormat:@"email=%@&password=%@",strUserName,strPass];
    
       SSRestManager *restManager = [[SSRestManager alloc]init];
       [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
           if(json){
               NSDictionary *res = [json valueForKey:@"data"];
               NSInteger status = [[json valueForKey:@"status"]integerValue];
               if(status==1){
                   NSString *strAdmin = [res valueForKey:@"is_admin"];
                   if([strAdmin isEqualToString:@"Y"]){
                       strAdmin = @"1";
                   }else{
                       strAdmin = @"0";
                   }
                    NSInteger id1 = [[res valueForKey:@"id"]integerValue];
                   NSString *strUid = [NSString stringWithFormat:@"%ld",(long)id1];
                    NSString *strUname = [res valueForKey:@"username"];
                    NSString *strEmail = [res valueForKey:@"email"];
                   NSString *strFname = [res valueForKey:@"firstname"];
                   
                   [[NSUserDefaults standardUserDefaults] setObject:strUid forKey:@"uid"];
                    [[NSUserDefaults standardUserDefaults] setObject:strUname forKey:@"uname"];
                   [[NSUserDefaults standardUserDefaults] setObject:strEmail forKey:@"email"];
                   [[NSUserDefaults standardUserDefaults] setObject:strAdmin forKey:@"isAdmin"];
                   [[NSUserDefaults standardUserDefaults] setObject:strFname forKey:@"fname"];
                   [[NSUserDefaults standardUserDefaults] synchronize];
                   handler(@"1",nil);
               }else{
                   handler(@"0",nil);
               }
           }
          } onError:^(NSError *error) {
              if (error) {
                  handler(nil,error);
              }
           
       }];

}
+(void)getAgentList:(NSString*)strUid onCompletion:(FetchCompletionHandler)handler{
//    NSString *strLoginUrl = [NSString stringWithFormat:@"%@%@",kBaseURL,kAgentList];
//    
//    NSString *kQuery = [NSString stringWithFormat:@"empid=%@",strUid];
//    SSRestManager *restManager = [[SSRestManager alloc]init];
//    [restManager getJsonResponseFromBaseUrl:strLoginUrl query:kQuery onCompletion:^(NSDictionary *json) {
//        if(json){
//            NSDictionary *res = [json valueForKey:@"result"];
//            NSArray *arrAgent = [res valueForKey:@"agentlist"];
//        NSMutableArray *arrAgentList = [[NSMutableArray alloc]init];
//          [arrAgent enumerateObjectsUsingBlock:^(NSDictionary *dict, NSUInteger idx, BOOL * _Nonnull stop) {
//              Agent *agent = [[Agent alloc]initWithDictionary:dict];
//              [arrAgentList addObject:agent];
//              
//          }];
////            for(int i=0;i<3;i++){
////                Agent *agent = [[Agent alloc]init];
////                agent= [agent createAgent:i];
////                [arrAgentList addObject:agent];
////            }
//            handler (arrAgentList,nil);
//        }
//    } onError:^(NSError *error) {
//        if (error) {
//            handler(nil,error);
//        }
//        
//    }];

}

@end
